###Application overview

- **Development environment**
	* X-Code 9.2
- **Dependency manager**
	* Cocoa Pods
- **Application architecture**
	* MVP. The interaction of all the parts of the Surf MVP 2.0 template must be strictly 	according to the interface (protocol). This is necessary for testing business logic 	using mock data.
- **Project structure**
	* The project root folder contains the following folders:
		- **Application**
		- **Library** - contains external, support files.
			**Extensions** - class extensions.
			**Utils** - utilities, helper classes, etc.
			**Protocols** - Protocols
		- **Models** - contains files associated with the model
		- **Resources** - application resources
		- **Screens** - application modules. Application contains two screens, 			QuotationScreen and OptionsScreen. So application contains two modules. Every 			module contains View, Presenter, Router, Configurator folders. Configurator 			create module view controller and binds the elements of the module to each 			other. Returns created view controller. Router use for transitions between 			modules. Presenter implements the business logic of the module and controls the 			display of data on the view. Presenter use services for work with socket and 			saved model entities. View just shows data and control users interaction.
		- **Services** - services for work with socket and storage/update models entity
