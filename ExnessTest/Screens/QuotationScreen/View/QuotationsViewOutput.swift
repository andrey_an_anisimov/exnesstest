//
//  QuotationsViewOutput.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 02.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

protocol QuotationsViewOutput {
    /// Notify presenter that view is ready
    func viewLoaded()
    func viewWillAppear()
    func closeSocket()
    func openSocket()
    func reorederList(list: [SavedQuotation])
    func reduceList(list: [SavedQuotation])
    func openOptionsView()
}
