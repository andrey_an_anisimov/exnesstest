//
//  QuotationCell.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 04.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

import UIKit

class QuotationCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak fileprivate var nameLabel: UILabel!
    @IBOutlet weak fileprivate var bidAskLabel: UILabel!
    @IBOutlet weak fileprivate var spreadLabel: UILabel!

    // MARK: - Constants
    private let offset = 3

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func fill(quotation: SavedQuotation) {
        let indexStartOfText = quotation.id.index(quotation.id.startIndex, offsetBy: offset)
        let substr2 = quotation.id[indexStartOfText...]
        let substr1 = quotation.id[..<indexStartOfText]
        nameLabel.text = substr1 + "/" + substr2
        bidAskLabel.text = quotation.bid + "/" + quotation.ask
        spreadLabel.text = quotation.spread
    }
    
}
