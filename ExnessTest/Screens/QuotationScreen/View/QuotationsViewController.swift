//
//  QuotationsViewController.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 02.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

import UIKit

final class QuotationsViewController: UIViewController, QuotationsViewInput, ModuleTransitionable {

    // MARK: - IBOutlets
    @IBOutlet weak fileprivate var tableView: UITableView!

    // MARK: - Properties
    var output: QuotationsViewOutput!
    fileprivate var adapter: QuotationsTableViewAdapter?
    fileprivate var isEditMode = false

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewLoaded()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        closeSocket()
        configureNavigationBar()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        output.viewWillAppear()
    }

    // MARK: - QuotationsViewInput

    func setupInitialState() {
        configureNavigationBar()
    }

    func prepareForData(list: [SavedQuotation]) {
        let adapter = QuotationsTableViewAdapter(tableView: tableView, list: list)
        tableView.delegate = adapter
        tableView.dataSource = adapter
        self.adapter = adapter
        tableView.reloadData()

        adapter.reduceListBlock = { [weak self] (list) in
            guard let `self` = self else { return }
            self.output.reduceList(list: list)
        }

        adapter.reorderListBlock = { [weak self] (list) in
            guard let `self` = self else { return }
            self.output.reorederList(list: list)
        }
    }

    func updateData(list: [SavedQuotation]) {
        adapter?.updateData(list: list)
    }


    func closeSocket() {
        output.closeSocket()
    }

    func openSocket() {
        output.openSocket()
    }
    // MARK: - Private helpers
    private func configureNavigationBar() {
        self.navigationItem.title = NSLocalizedString("Quotations", comment: "")
        let leftBarButtonItem = UIBarButtonItem(
            title: NSLocalizedString("Edit", comment: ""),
            style: .plain,
            target: self,
            action: #selector(self.editMode)
        )

        let rightBarButtonItem = UIBarButtonItem(
            title: NSLocalizedString("Options", comment: ""),
            style: .plain,
            target: self,
            action: #selector(self.openOptionsView)
        )
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }

    private func updateLeftButton() {
        let leftBarButtonItem: UIBarButtonItem?
        if isEditMode {
            leftBarButtonItem = UIBarButtonItem(
                title: NSLocalizedString("Cancel", comment: ""),
                style: .plain,
                target: self,
                action: #selector(self.editMode)
            )
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        } else {
            leftBarButtonItem = UIBarButtonItem(
                title: NSLocalizedString("Edit", comment: ""),
                style: .plain,
                target: self,
                action: #selector(self.editMode)
            )
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }

    @objc
    private func editMode() {
        isEditMode = !isEditMode
        updateLeftButton()
        adapter?.updateEditState()
    }

    @objc
    private func openOptionsView() {
        output.openOptionsView()
    }

}
