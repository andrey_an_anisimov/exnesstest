//
//  QuotationsTableViewAdapter.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 04.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

import UIKit

class QuotationsTableViewAdapter: NSObject {

    // MARK: - Properties
    fileprivate var list: [SavedQuotation]
    fileprivate let tableView: UITableView
    fileprivate var editMode = false

    var reorderListBlock: ChangedListBlock?
    var reduceListBlock: ChangedListBlock?
    // MARK: - Constants
    fileprivate let cellHeight: CGFloat = 44.0
    fileprivate let numberOfSections = 1


    init(tableView: UITableView, list: [SavedQuotation]) {
        tableView.registerNib(cellType: QuotationCell.self)
        self.list = list
        self.tableView = tableView
    }

    func updateData(list: [SavedQuotation]) {
        if !editMode {
            self.list = list
            tableView.reloadData()
        }
    }

    func updateEditState() {
        editMode = !editMode
        tableView.setEditing(editMode, animated: true)
        tableView.reloadData()
    }
}

extension QuotationsTableViewAdapter: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = QuotationTableViewHeader.fromXib() as? QuotationTableViewHeader
        return headerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if editMode {
            return 0
        }
        return cellHeight
    }
}

extension QuotationsTableViewAdapter: UITableViewDataSource {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: QuotationCell.nameOfClass) as? QuotationCell else {
            return UITableViewCell()
        }
        let item = list[indexPath.row]
        cell.fill(quotation: item)
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }

    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }

    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let item = list[sourceIndexPath.row]
        list.remove(at: sourceIndexPath.row)
        list.insert(item, at: destinationIndexPath.row)
        reorderListBlock?(list)
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            list.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            reduceListBlock?(list)
        }
    }
}

