//
//  QuotationsRouter.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 02.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

import UIKit

final class QuotationsRouter: QuotationsRouterInput {

	// MARK: Properties

    weak var view: ModuleTransitionable?

	// MARK: QuotationsRouterInput
    func openOptionsScreen() {
        let optionsViewController = OptionsModuleConfigurator().configure
        view?.push(module: optionsViewController(), animated: true, hideTabBar: false)
    }
}
