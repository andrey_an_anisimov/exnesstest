//
//  QuotationsModuleConfigurator.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 02.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

import UIKit

final class QuotationsModuleConfigurator {

    // MARK: Internal methods

    func configure() -> QuotationsViewController {
        let view = QuotationsViewController()
        let presenter = QuotationsPresenter()
        let router = QuotationsRouter()

        presenter.view = view
        presenter.router = router
        router.view = view
        view.output = presenter

        return view
    }

}
