//
//  QuotationsPresenter.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 02.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//
import Starscream
import ObjectMapper
final class QuotationsPresenter: QuotationsViewOutput, QuotationsModuleInput {

    // MARK: - Properties

    weak var view: QuotationsViewInput!
    var router: QuotationsRouterInput!

    var service: SocketService?
    var savedService: SavedQuotationService?
    // MARK: - QuotationsViewOutput

    func viewLoaded() {
        view.setupInitialState()
        savedService = SavedQuotationService()
        savedService?.initAllInstruments()
}

    func viewWillAppear() {
        openSocket()
    }

    func openSocket() {
        service = SocketService()
        service?.connect { [weak self] (result) in
            guard let `self` = self else { return }
            if result {
                if let selectedQuotations = self.savedService?.getSelectedQuotations() {
                    var subscribeMessage = SocketMessage.subscribe
                    for item in selectedQuotations {
                        subscribeMessage.append(item.id)
                        if item.id != selectedQuotations.last?.id {
                            subscribeMessage.append(",")
                        }
                    }
                    self.view.prepareForData(list: selectedQuotations)
                    self.service?.subscribe(message: subscribeMessage)
                    self.service?.handleSocketString(completion: { (text) in
                        let result = Mapper<SubscriberResult>().map(JSONString: text)
                        if let items = result?.quotations {
                            self.savedService?.updateSavedValue(quotations: items, completion: { (value) in
                                if let selQuotations = self.savedService?.getSelectedQuotations(), value {
                                    self.view.updateData(list: selQuotations)
                                }
                            })
                        }
                    })
                }
            }
        }
    }

    func reorederList(list: [SavedQuotation]) {
        savedService?.reorderSavedQuotations(list: list)
    }

    func reduceList(list: [SavedQuotation]) {
        savedService?.reduceSavedQuotations(list: list)
    }

    func closeSocket() {
        service?.disconnect()
    }

    func openOptionsView() {
        router.openOptionsScreen()
    }
    // MARK: - QuotationsModuleInput

}
