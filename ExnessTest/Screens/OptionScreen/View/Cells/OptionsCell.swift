//
//  OptionsCell.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 05.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

import UIKit

class OptionsCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak fileprivate var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func fill(quotation: SavedQuotation) {
        nameLabel.text = quotation.id
        if quotation.isSelected {
            self.accessoryType = .checkmark
        } else {
            self.accessoryType = .none
        }
    }
}
