//
//  OptionsViewController.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 02.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

import UIKit

final class OptionsViewController: UIViewController, OptionsViewInput, ModuleTransitionable {

    // MARK: - IBOutlets
    @IBOutlet weak fileprivate var tableView: UITableView!

    // MARK: - Properties

    var output: OptionsViewOutput!
    fileprivate var adapter: QptionsTableViewAdapter?

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewLoaded()
    }

    // MARK: - OptionsViewInput

    func setupInitialState() {
        configureNavigationBar()
    }

    func prepareForData(list: [SavedQuotation]) {
        let adapter = QptionsTableViewAdapter(tableView: tableView, list: list)
        tableView.delegate = adapter
        tableView.dataSource = adapter
        self.adapter = adapter
        tableView.reloadData()

        adapter.selectedBlock = { [weak self] (item) in
            guard let `self` = self else { return }
            self.output.updateSelecttionState(item: item)
        }
    }

    func updateData(list: [SavedQuotation]) {
        adapter?.updateData(list: list)
    }
    // MARK: - Private helpers
    private func configureNavigationBar() {
        self.navigationItem.title = NSLocalizedString("Options", comment: "")

    }

}
