//
//  OptionsViewOutput.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 02.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

protocol OptionsViewOutput {
    /// Notify presenter that view is ready
    func viewLoaded()
    func updateSelecttionState(item: SavedQuotation)
}
