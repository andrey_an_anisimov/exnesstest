//
//  OptionsViewInput.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 02.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

protocol OptionsViewInput: class {
    /// Method for setup initial state of view
    func setupInitialState()
    func prepareForData(list: [SavedQuotation])
    func updateData(list: [SavedQuotation])
}
