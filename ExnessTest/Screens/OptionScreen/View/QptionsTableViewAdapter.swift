//
//  QptionsTableViewAdapter.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 05.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

import UIKit

class QptionsTableViewAdapter: NSObject {

    // MARK: - Properties
    fileprivate var list: [SavedQuotation]
    fileprivate let tableView: UITableView
    var selectedBlock: SelectedBlock?
    // MARK: - Constants
    fileprivate let cellHeight: CGFloat = 44.0
    fileprivate let numberOfSections = 1

    init(tableView: UITableView, list: [SavedQuotation]) {
        tableView.registerNib(cellType: OptionsCell.self)
        self.list = list
        self.tableView = tableView
    }

    func updateData(list: [SavedQuotation]) {
        self.list = list
        tableView.reloadData()
    }
}

extension QptionsTableViewAdapter: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let item = list[indexPath.row]
        selectedBlock?(item)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
}

extension QptionsTableViewAdapter: UITableViewDataSource {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: OptionsCell.nameOfClass) as? OptionsCell else {
            return UITableViewCell()
        }
        let item = list[indexPath.row]
        cell.fill(quotation: item)
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }
}

