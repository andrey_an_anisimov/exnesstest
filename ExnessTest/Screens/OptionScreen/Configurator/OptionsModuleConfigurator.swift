//
//  OptionsModuleConfigurator.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 02.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

import UIKit

final class OptionsModuleConfigurator {

    // MARK: Internal methods

    func configure() -> OptionsViewController {
        let view = OptionsViewController()
        let presenter = OptionsPresenter()
        let router = OptionsRouter()

        presenter.view = view
        presenter.router = router
        router.view = view
        view.output = presenter

        return view
    }

}
