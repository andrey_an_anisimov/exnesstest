//
//  OptionsPresenter.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 02.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

final class OptionsPresenter: OptionsViewOutput, OptionsModuleInput {

    // MARK: - Properties

    weak var view: OptionsViewInput!
    var router: OptionsRouterInput!
    var savedService: SavedQuotationService?

    // MARK: - OptionsViewOutput

    func viewLoaded() {
        view.setupInitialState()
        savedService = SavedQuotationService()
        if let list = savedService?.getAllQuotations() {
            view.prepareForData(list: list)
        }
    }

    func updateSelecttionState(item: SavedQuotation) {
        savedService?.updateSelectedState(item: item)
        if let list = savedService?.getAllQuotations() {
            view.updateData(list: list)
        }
    }
    // MARK: - OptionsModuleInput

}
