//
//  SocketSocketService.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 02.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

import Starscream
/// Service class for work with socket
class SocketService {

    private var socket: WebSocket?
    init() {
        if let url = URL(string: URLs.socketUrl), socket == nil {
            socket = WebSocket(url: url)
        }
    }
    /// This method used for connect to web socket.
    ///
    /// - Parameters: completion block, return true, if connection success.
    func connect(completion: @escaping SocketConnectBlock) {
        socket?.onConnect = {
            print("websocket is connected")
            completion(true)
        }
        socket?.disconnect()
        socket?.connect()
    }
    /// This method used for handle recived from socket data.
    ///
    /// - Parameters: completion block, return message text if connection success.
    func handleSocketString(completion: @escaping SocketGetTextBlock) {
        if let connected = socket?.isConnected, connected {
            socket?.onText = { (text) in
                completion(text)
            }
        }
    }
    /// This method used for quotations subcribe .
    ///
    /// - Parameters: String, contains command.
    func subscribe(message: String) {
        if let connected = socket?.isConnected, connected {
            socket?.write(string: message)
        }
    }
    /// This method used for quotations unsubcribe .
    ///
    /// - Parameters: String, contains command.
    func unsubscribe(message: String) {
        if let connected = socket?.isConnected, connected {
            socket?.write(string: message)
        }
    }
    /// This method used for socket disconnect (inactive app state, Optuins screen)  .
    func disconnect() {
        if let connected = socket?.isConnected, connected {
            socket?.disconnect()
            socket = nil
        }
        socket = nil
    }
}


