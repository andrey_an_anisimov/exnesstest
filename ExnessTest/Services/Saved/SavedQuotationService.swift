//
//  SavedQuotationService.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 03.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

import UIKit
import RealmSwift
/// Service class for work with realm (saved) objects
class SavedQuotationService {

    private struct Keys {
        public static let isFirstRun = "firstRun"
    }

    /// This method used for init all possible quotations at first time after app. start.
    func initAllInstruments() {
        let defaults = UserDefaults.standard
        if !defaults.bool(forKey: Keys.isFirstRun) {
            defaults.set(true, forKey: Keys.isFirstRun)
            saveInstruments()
        }
    }
    /// This method used for get all selected quotations.
    ///
    /// - Returns: selected quotations array.
    func getSelectedQuotations() -> [SavedQuotation] {
        do {
            let realm = try Realm()
            let predicate = NSPredicate(format: "isSelected = true")
            let quotations = realm.objects(SavedQuotation.self).filter(predicate).sorted(byKeyPath: "sortOrder", ascending: true)
            return quotations.toArray()
        } catch {
            print("Could not access database: ", error)
        }
        return []
    }
    /// This method used for get all possible (saved) quotations.
    ///
    /// - Returns: quotations array.
    func getAllQuotations() -> [SavedQuotation] {
        do {
            let realm = try Realm()
            let quotations = realm.objects(SavedQuotation.self)
            return quotations.toArray()
        } catch {
            print("Could not access database: ", error)
        }
        return []
    }
    /// This method used for save new selected quotations order.
    ///
    /// - Parameters: reorderd array.
    func reorderSavedQuotations(list: [SavedQuotation]) {
        var sorted = list.map {$0.sortOrder}
        sorted.sort()
        do {
            let realm = try Realm()
            var index = 0
            for item in list {
                try realm.write {
                    item.sortOrder = sorted[index]
                }
                index += 1
            }
        } catch {
            print("Could not access database: ", error)
        }
    }
    /// This method used for save new selected quotations list after delete row.
    ///
    /// - Parameters: reduced array.
    func reduceSavedQuotations(list: [SavedQuotation]) {
        let full = getAllQuotations()

        do {
            let realm = try Realm()
            for item in full {
                try realm.write {
                    if list.index(of: item) == nil {
                        item.isSelected = false
                    }
                }
            }
        } catch {
            print("Could not access database: ", error)
        }
    }
    /// This method used for save new selected/unselected quotation.
    ///
    /// - Parameters: SavedQuotation.
    func updateSelectedState(item: SavedQuotation) {
        do {
            let realm = try Realm()
            try realm.write {
                item.isSelected = !item.isSelected
            }
        } catch {
            print("Could not access database: ", error)
        }
    }
    /// This method used for save quotation parameters, recived from socket
    ///
    /// - Parameters: array of plain object, completion block.
    func updateSavedValue(quotations: [Quotation], completion: @escaping ReadyUpdateBlock) {
        do {
            let realm = try Realm()
            for item in quotations {
                let predicate = NSPredicate(format: "id = %@ AND isSelected = true", item.id)
                let quotation = realm.objects(SavedQuotation.self).filter(predicate).first
                try realm.write {
                    quotation?.ask = item.ask
                    quotation?.bid = item.bid
                    quotation?.spread = item.spread
                }
            }
            completion(true)
        } catch {
            print("Could not access database: ", error)
        }
    }
    // MARK: - Private helpers
    private func saveInstruments() {
        var order = 0
        do {
            let realm = try Realm()
            for name in allInstrumentsName() {
                let quotation = SavedQuotation()
                quotation.isSelected = true
                quotation.sortOrder = order
                quotation.id = name
                try realm.write {
                    realm.add(quotation, update: true)
                }
                order += 1
            }
        }
        catch {
            print("Could not access database: ", error)
        }
    }

    private func allInstrumentsName() -> [String] {
        return ["EURUSD", "EURGBP", "USDJPY",
                "GBPUSD", "USDCHF", "USDCAD",
                "AUDUSD", "EURJPY","EURCHF"]
    }
}

extension Results {

    func toArray() -> [T] {
        var array = [T]()
        for result in self {
            array.append(result)
        }
        return array
    }
}
