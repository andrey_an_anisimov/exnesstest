//
//  SavedQuotation.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 03.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

import UIKit
import RealmSwift
/// This class is a realm entity, use for save Qutation state
class SavedQuotation: Object {

    @objc dynamic var id = ""
    @objc dynamic var sortOrder = 0
    @objc dynamic var bid = ""
    @objc dynamic var ask = ""
    @objc dynamic var spread = ""
    @objc dynamic var isSelected = true

    override static func primaryKey() -> String? {
        return "id"
    }
}
