//
//  SubscriberResult.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 03.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

import ObjectMapper
/// This class is a plain entity, support map data from socket
class SubscriberResult: Mappable {

    private struct Keys {
        public static let quotation = "ticks"
        public static let subscribedList = "subscribed_list"
    }

    public var quotations: [Quotation]?

    public required init?(map: Map) {
        if let dict = map.JSON[Keys.subscribedList],
            let innerDict = dict as? [String: Any],
            let items = innerDict[Keys.quotation] {
                self.quotations = Mapper<Quotation>().mapArray(JSONObject: items)
        } else if let items = map.JSON[Keys.quotation] {
            self.quotations = Mapper<Quotation>().mapArray(JSONObject: items)
        }
    }

    public func mapping(map: Map) {
    }
}
