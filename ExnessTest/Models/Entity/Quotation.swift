//
//  Quotation.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 03.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

import ObjectMapper
/// This class is a plain entity, data received from socket mapped to this
/// class
class Quotation: Mappable {

    private struct Keys {
        public static let id = "s"
        public static let bid = "b"
        public static let ask = "a"
        public static let spread = "spr"
    }

    public var id: String  = ""
    public var bid: String  = ""
    public var ask: String  = ""
    public var spread: String  = ""

    public required init?(map: Map) {
    }

    public func mapping(map: Map) {
        self.id <- map[Keys.id]
        self.bid <- map[Keys.bid]
        self.ask <- map[Keys.ask]
        self.spread <- map[Keys.spread]
    }
}
