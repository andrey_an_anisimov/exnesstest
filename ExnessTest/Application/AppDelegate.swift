//
//  AppDelegate.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 02.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let viewController = QuotationsModuleConfigurator().configure()
        self.window?.rootViewController = UINavigationController(rootViewController: viewController)
        self.window?.backgroundColor = UIColor.white
        self.window?.makeKeyAndVisible()
        return true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
       socketOpenClose(needOpen: false)
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        socketOpenClose(needOpen: true)
    }
    
    private func socketOpenClose(needOpen: Bool) {
        let nvc = self.window?.rootViewController as! UINavigationController
        if nvc.viewControllers.count == 1 {
            let vc = nvc.viewControllers[0] as! QuotationsViewController
            if needOpen {
                vc.openSocket()
            } else {
                vc.closeSocket()
            }
        }
    }
}

