//
//  UIView.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 02.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

import Foundation
import UIKit

public extension UIView {

    class func fromXib() -> UIView {
        guard let view = Bundle.main.loadNibNamed(nameOfClass, owner: nil, options: nil)?.first as? UIView else {
            return UIView()
        }
        return view
    }

    class func fromXib(_ name: String? = nil) -> Self? {
        return instancetype(object: Bundle.main.loadNibNamed(name ?? nameOfClass, owner: nil, options: nil)?.last)
    }

}
