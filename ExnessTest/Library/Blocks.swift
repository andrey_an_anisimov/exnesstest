//
//  Blocks.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 04.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//
import Foundation
/// typealiases using in block
typealias ReadyUpdateBlock = (Bool) -> Void

typealias SocketConnectBlock = (Bool) -> Void
typealias SocketGetTextBlock = (String) -> Void
typealias SocketGetDataBlock = (Data) -> Void

typealias ChangedListBlock = ([SavedQuotation]) -> Void
typealias SelectedBlock = (SavedQuotation) -> Void
