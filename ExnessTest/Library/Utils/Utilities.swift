//
//  Utilities.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 04.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

import Foundation
import UIKit
public class Utilities {

    public static func lerp(x: Double, a0: Double, b0: Double, a1: Double, b1: Double) -> Double {
        var part = (x - a0) / (b0 - a0)
        if part > 1 {
            part = 1
        } else if part < 0 {
            part = 0
        }
        return part * (b1 - a1)
    }

    public static func iterateEnum<T: Hashable>(_: T.Type) -> AnyIterator<T> {
        var i = 0
        return AnyIterator {
            let next = withUnsafeBytes(of: &i) { $0.load(as: T.self) }
            if next.hashValue != i { return nil }
            i += 1
            return next
        }
    }
}

public func instancetype<T>(object: Any?) -> T? {
    return object as? T
}


