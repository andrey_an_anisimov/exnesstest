//
//  Constants.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 04.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

import UIKit

enum SocketMessage {
    static let subscribe = "SUBSCRIBE: "
}
