//
//  URLs.swift
//  ExnessTest
//
//  Created by Andrey Anisimov on 02.01.2018.
//  Copyright © 2018 Andrey Anisimov. All rights reserved.
//

import UIKit

class URLs {
    public static let socketUrl = "wss://quotes.exness.com:18400"
}
